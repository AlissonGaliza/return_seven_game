extends Node

var rooms = [['','','','','',''],['','','','','',''],['','','','','',''],
['','','','','',''],['','','','','',''],['','','','','','']]

func _ready():
	#var playerCamera = get_tree().get_root().get_node("Game/player/PlayerCamera")
	#playerCamera.current = false;
	#$MapaCamera.current = true;
	
	for y in range (6):
		var row = rooms[y]
		for x in range (6):
			if row[x] != '':
				
				var room = preload("res://map_generator/map_generator.tscn").instance()
				var room_pos = Vector2(224*x, 224*y) + Vector2(-224, -224)
				room.position = room_pos
				room.name = row[x]
				
				add_child(room)

func _init():
	var allRooms = pickRoomTypes()

	randomize()
	var x= 1
	var y = 1
	for i in range(1000):
		if rooms[x][y] == '':
			if len(allRooms) < 1:
				break;
			else:
				rooms[x][y] = allRooms.pop_front() 
		var dir = (randi() % 4) * 90
		x += lengthdir_x(1, dir)
		y += lengthdir_y(1, dir)
		x = clamp(x,0,5)
		y = clamp(y,0,5)


	print(rooms)

func lengthdir_x(length, direction):
	return length*cos(direction)

func lengthdir_y(length, direction):
	return length*sin(direction)

func pickRoomTypes():
	
	var fightingRooms = 5;
	var puzzleRooms = 2;
	var loreRooms = 2;
	var bossRoom = 1;
	var totalRooms = fightingRooms + puzzleRooms + loreRooms + bossRoom;
	var roomType = [fightingRooms,puzzleRooms,loreRooms,bossRoom]
	var allRooms = [];
	
	for i in range(1000): #colocando as salas em um array 
		var randomType = randi() % 4;
		if roomType[randomType] >= 1:
			roomType[randomType] -= 1;
			match randomType:
				0: allRooms.append('fightingRoom')
				1: allRooms.append('puzzleRoom')
				2: allRooms.append('loreRoom')
				3: allRooms.append('bossRoom')
		
		elif roomType[0] == 0 and roomType[1] == 0 and roomType[2] == 0 and roomType[3] == 0:
			break; #todas as salas foram adicionadas
	return allRooms;

