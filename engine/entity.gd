extends KinematicBody2D

const TYPE = "ENEMY"
const  SPEED = 0;

var movedir = dir.center
var knockdir = dir.center

var hitstun = 0
var health = 1

var mov_direction= dir.center;
var sprite_direction = "down";

func movement_loop():
	var motion 
	if hitstun == 0:
		motion = mov_direction.normalized() * SPEED
	else:
		motion = knockdir.normalized() * 125
	
	move_and_slide(motion, dir.center)

func sprite_direction_loop():
	match mov_direction:
		dir.left:
			sprite_direction = "left" 
		dir.right:
			sprite_direction = "right"
		dir.down:
			sprite_direction = "down"
		dir.up:
			sprite_direction = "up"
	
func animation_switch(animation):
	var new_anim = str(animation, sprite_direction)
	if $anim.current_animation != new_anim:
		$anim.play(new_anim);
		print(new_anim)
		
		
func damage_loop():
	if hitstun > 0:
		hitstun -= 1
	for area in $hitbox.get_overlapping_areas():
		var body = area.get_parent()
		if hitstun == 0 and body.get("DAMAGE") != null and body.get("TYPE") != TYPE:
			health -= body.get("DAMAGE")
			hitstun = 10
			knockdir = global_transform.origin - body.global_transform.origin
			
func use_item(item):
	var newItem = item.instance()
	newItem.add_to_group(str(newItem.get_name(), self))
	add_child(newItem);
	if get_tree().get_nodes_in_group(str(newItem.get_name(), self)).size() > newItem.maxamount:
		newItem.queue_free()