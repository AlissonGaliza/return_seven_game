extends "res://engine/entity.gd"

const DAMAGE = 1;
const SPEED = 40;

var movetimer_leght = 15;
var movetimer = 0;

func _ready():
	$anim.play("default")
	mov_direction = dir.rand();

func _physics_process(delta):
	movement_loop();
	if movetimer > 0:
		movetimer -= 1;
	elif movetimer == 0 || is_on_wall():
		mov_direction = dir.rand();
		movetimer = movetimer_leght;
	damage_loop()