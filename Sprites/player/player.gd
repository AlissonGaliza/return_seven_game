extends "res://engine/entity.gd"

const TYPE = "PLAYER";
const SPEED = 70;

var state = "default"



func _physics_process(delta):
	match state:
		"default": state_default()
		"idle": state_swing()
	
func state_default():
	controls_loop()
	movement_loop()
	sprite_direction_loop()
	damage_loop()
	
	if(is_on_wall()):
		if sprite_direction == "left" and test_move(transform, dir.left):
			animation_switch("pushing")
		if sprite_direction == "right" and test_move(transform, dir.right):
			animation_switch("pushing")
		if sprite_direction == "up" and test_move(transform, dir.up):
			animation_switch("pushing")
		if sprite_direction == "down" and test_move(transform, dir.down):
			animation_switch("pushing")
	elif mov_direction != dir.center:
		animation_switch("walk")
	else:
		animation_switch("idle")
		
	if Input.is_action_just_pressed("a"):
		use_item(preload("res://items/sword/sword.tscn"))

func state_swing():
	anim_switch("idle")
	movement_loop()
	damage_loop()
	movedir = dir.center

func controls_loop():
	var LEFT	= Input.is_action_pressed("ui_left")
	var RIGHT	= Input.is_action_pressed("ui_right")
	var UP	= Input.is_action_pressed("ui_up")
	var DOWN	= Input.is_action_pressed("ui_down")
	
	mov_direction.x = -int(LEFT) + int(RIGHT)
	mov_direction.y = -int(UP) + int(DOWN)


